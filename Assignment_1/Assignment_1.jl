### A Pluto.jl notebook ###
# v0.14.5

using Markdown
using InteractiveUtils

# ╔═╡ 72641af2-af36-11eb-060b-377776bf04a4
begin 
	using Pkg
	Pkg.activate("project.toml")
	using PlutoUI
end

# ╔═╡ 1d119b9b-138f-4465-b866-b7383aa69312
begin
	mutable struct office
		Id::String
		itemCount::Int64
		neighbourW::Bool
		neighbourE::Bool
		index::Int64
	end
	
	struct Action
		Name::String
		Cost::Int64
	end
	
   mutable struct location
		current_O::office
		prev_O::office
		prev_A::Action
		#next_O::office
		
	end	
	
	mutable struct state 
		I_Count::Int64
		S_Cost::Int64
		office_Lot::Vector{Int64}
		A_position::Int64
		huristicVal::Int64
	end
	
	Wo 	= office("West_Office", 1, false,true,1)
	C1 	= office("C1_Office", 3, true,true,2)
	C2 	= office("C2_Office", 2, true,true,3)
	Eo 	= office("East_Office", 1, true,false,4)
	
	Re = Action("Remain", 1)
	Me = Action("Move East", 3)
	Mw = Action("Move West", 3)
	Co = Action("Collect", 5)
	
	global Current_position = location(C1,C1,Re)
	global evaluate = true
	
	ItemCount = 7
	officeList = [Wo,C1,C2,Eo]
	TransitionModel = Dict()
	moves = []
	
	availableActions = Action[Co,Re,Me,Mw]
	currentBalance = 5
	stateList = []
	stuffInOffices = [1,3,2,1]
	
	# global c_State = state(ItemCount,currentBalance,stuffInOffices,Current_position.current_O.index, 0)
	global c_State = state(6, 5, [1,2,2,1],2,11) #
	global nodeList = state[]
	global closedList = state[]
	
	
	
end 


# ╔═╡ 286ec198-8005-44e5-b3fb-8e54f48b6709
#= if Current_position.prev_A != Re && current_O.itemCount != 0
		push!(actionQue, Re)
end
if Current_position.prev_A != Co && current_O.itemCount != 0 =#

# ╔═╡ 8b9602bc-9ef3-410a-9379-079e373cb375
#=
function move(Action, Old_Location)
	#	currentBalance != 0 #Wont run if No Moves made yet
	
	#Move West
		if Action.Name == "Move West" && Old_Location.current_O.neighbourW == true
#=	
		if Old_Location.prev_A == Me && Old_Location.prev_O.itemCount == 0 
			push!(moves, "Usless Move West")
			return
		end
		
=#
		index = Old_Location.current_O.index #Record Index
		
		Old_Location.prev_O = officeList[index] #Record Office Being Left
		
		Old_Location.prev_A = Action #Record Action taken
		
		Old_Location.current_O = officeList[index - 1] #Record Current 
		
		Current_position = Old_Location # Change Location
		
		currentBalance.value = currentBalance.value + Action.Cost #add Cost
		
		push!(moves, Action.Name) #Track movement
		
		#Build New State
		
		#push!(moves, string("New Cost ", currentBalance.value))
		return
	elseif Action.Name == "Move West" && Old_Location.current_O.neighbourW == false
	push!(moves, string("West Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	#Move East
		if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
		if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
			push!(moves, "Usless Move East")
			return
		end
=#
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		#push!(moves, string("New Cost ", currentBalance.value))
		return

	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	
	
	#Collect Action
		if Action.Name == "Collect"
	
		if  Old_Location.prev_A == Co
			push!(moves, "Cannot Collect Twice")
			return
	end
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Office Is Empty")
			return
	end				
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end			
			
	#Remain Action
		if Action.Name == "Remain"
			
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Cannot Remain In Empty Office")
			return
	end		
		if  Old_Location.prev_A == Re
			push!(moves, "Cannot Remain Twice")
			return
	end
			
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end	
			
		#=			
		push!(moves, "1 Item Collected")
		
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		#push!(moves, string("New Cost ", currentBalance.value))
		return
	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed =>", Old_Location.current_O.index))
		return	
			=#	
			
		end
		end
	end
=#

# ╔═╡ 404acaa3-9b99-47f5-8279-4fa7fd2fdf74
function start(c_State)
	
	#	currentBalance != 0 #Wont run if No Moves made yet
	T_state = state()
	
	#Temporary Values To build new States (nodes)
	T_position =  Current_position
	T_Balance = currentBalance
	T_CT = ItemCount
	location = Current_position.current_O.index
	
	for actionz in availableActions
			#Move West
		if actionz == Mw && Current_position.current_O.neighbourW == true
		
		index = Current_position.current_O.index #Record Index
		
		T_position.prev_O = officeList[index] #Record Office Being Left
		
		T_position.prev_A = Mw #Record Action taken
		
		T_position.current_O = officeList[index - 1] #Record Current 
		
		 # Change Location
		
		T_Balance = currentBalance.value + Mw.Cost #add Cost
		
		push!(moves, Action.Name) #Track movement
			
			
		#Build New State
		
		T_state.I_Count = T_CT
			
		T_state.S_Cost = T_Balance
			
		T_state.office_Lot = c_State.office_Lot
		
		T_state.A_position = T_position.current_O.index
			
		T_state.huristicVal = ItemCount  + T_Balance
		
		# for o in officeLot
		# 	push!(T_state.office_Lot, c_State.office_Lot)
		# end
			
			
		return
end
	
	end #End For Loop
	
	#Move East
		if Action.Name == "Move East" && Old_Location.current_O.neighbourE == true
#=
		if Old_Location.prev_A == Mw && Old_Location.prev_O.itemCount == 0 Old_Location.current_O.neighbourW == false
			push!(moves, "Usless Move East")
			return
		end
=#
		index = Old_Location.current_O.index
		Old_Location.prev_O = officeList[index]
		Old_Location.prev_A = Action
		Old_Location.current_O = officeList[index + 1]
		Current_position = Old_Location
		currentBalance.value = currentBalance.value + Action.Cost
		push!(moves, Action.Name) #Track movement
		push!(actionQue, Action.Name)
		#push!(moves, string("New Cost ", currentBalance.value))
		return

	elseif Action.Name == "Move East" && Old_Location.current_O.neighbourE == false
	push!(moves, string("East Move Not Allowed => ", Old_Location.current_O.index))
		return
end
	
	
	#Collect Action
		if Action.Name == "Collect"
	
		if  Old_Location.prev_A == Co
			push!(moves, "Cannot Collect Twice")
			return
	end
		
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Office Is Empty")
			return
	end				
		
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
		
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
		
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end			
			
	#Remain Action
		if Action.Name == "Remain"
			
		if Old_Location.current_O.itemCount == 0
			push!(moves, "Cannot Remain In Empty Office")
			return
	end		
			
		if  Old_Location.prev_A == Re
			push!(moves, "Cannot Remain Twice")
			return
	end
			
				    #If West Office
		if  Wo == Old_Location.current_O
			 Wo.itemCount = Wo.itemCount - 1;
				return
				end
					#If C1 Office
		if C1 == Old_Location.current_O
		     	C1.itemCount = C1.itemCount - 1;
				return
				end 
					#If C2 Office
		if C2 == Old_Location.current_O
				 C2.itemCount = C2.itemCount - 1;
				return
				end		
					#If East Office
		if E == Old_Location.current_O
				 E.itemCount = E.itemCount - 1;
				return
				end	
			end
		end
	
	# currentBalance = 
	Current_position = T_position
	end

# ╔═╡ e834f65e-0db3-4f84-bfd4-f6650c65312a
# move()

# ╔═╡ fdd6e435-cf61-4720-92d9-270f545baca1

function StateTracker(H_Val, C_Val, office_Lot, A_position)
	if evaluate != true 
		N_state = state(H_Val,C_Val )
		push!(nodeList, N_state)
	
	end
end


# ╔═╡ d4776e5f-5a53-479c-8965-331f91380f73
# function get_Heuristic(ItemCount, totalCost, action)
# 	if action == Co 
# 		x = Item
# end

# ╔═╡ 6b7b89bc-4eb6-42e8-ac8c-fc9e05a04828
begin
	Cr = state(6, 6, [1,2,2,1],2,12) #
	Cw = state(6, 8, [1,2,2,1],1,14) #
	Ce = state(6, 8, [1,2,2,1],3,14) #
end

# ╔═╡ 805542fa-ff1e-4bc8-9a18-c6fe1ee91fda
begin 
	push!(nodeList, Cr)
	push!(nodeList, Cw)
	push!(nodeList, Ce)
end

# ╔═╡ 89b1320e-27cc-41d1-9028-4d641aa593b6
with_terminal() do
	println(nodeList)
end

# ╔═╡ 6ab1f0f3-de2d-4b8b-93fb-185dc30e073c
# for node in nodeList
# 	if openList(node)
# 		# show()
# 		return
# end

# ╔═╡ 62553898-4d5f-41a4-a07d-87db9f5b4519
	function evaluateOpenList(c_State)
	T_cost = currentBalance
	T_ItemCount = c_State.I_Count
         if evaluate == true
		
		for node in nodeList
			
			if node.I_Count != T_ItemCount
				
				if node.I_Count < T_ItemCount
					push!(closedList, node)
					 c_State = node
					push!(moves, node)
					return
				end
				
			end
			
		end
		
		for node in nodeList
		push!(moves, node.I_Count == T_ItemCount)
			if node.I_Count == T_ItemCount
						push!(moves,string("h=>",node.huristicVal, "h2=>",T_cost))
				if node.huristicVal <= T_Hval
					T_Hval = node.huristicVal
					 c_State = node
					push!(closedList, node)
					push!(moves, "low")
					return
				end
				
			end
			
		end
		
		
		
		
	end
end
	

# ╔═╡ 338783ab-1ea8-4956-883d-14d9a39af543
 function openList(aState)

	if evaluate == false
		push!(nodeList, N_state)
	return true
	elseif evalute == true
		evaluateOpenList()
	end
end

# ╔═╡ 4e8d81dc-6e9f-42de-9ef9-24664ada11b5
evaluateOpenList(c_State)

# ╔═╡ 281ee45c-20ca-4e43-a885-7f78987acb44
with_terminal() do
	println(moves)
end

# ╔═╡ 7cb1530a-4e45-4513-8f62-69ca5d33dc43
with_terminal() do
	println(closedList)
	
end

# ╔═╡ 307ffb1a-edfb-4bcf-a675-43315d01b700
with_terminal() do
	println(c_State)
	
end

# ╔═╡ 38cd1fc8-b6e8-4fab-a034-95b977bc3c27
 #=
begin
	move(Mw,Current_position)
	move(Mw,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
	move(Me,Current_position)
end
=#	
#move(Mw,Current_position)
	#move(Me,Current_position)
	#move(Co,Current_position)
	#move(Re,Current_position)
#=
#wont work here
with_terminal() do
   println(moves)
end=#

# ╔═╡ 47ae0816-9368-43e3-b153-6cff476612ed
#=with_terminal() do
		println(officeList)
end=#

# ╔═╡ b1e9d358-e009-46ad-8d31-f00af4d4cdae
# with_terminal() do 
# 	println(string(
# 			"Action Taken => ",  moves,"\n",
# 			"Location => ",Current_position.current_O.Id, "\n",
# 			"Previous Node => ", Current_position.prev_O.Id,"\n",
# 			"Previous Action => ", Current_position.prev_A.Name,"\n",
# 			"Office Arrangement =>", "\n",
# 			"Cost To Get Here => ", currentBalance))
# end

# ╔═╡ 74950239-9b5e-4110-ae4f-31478770d8c5
#=R = state(7, 1, [1,3,2,1],2)


begin

	start = state(7, 0, [1,3,2,1], 2)

	
	C = state(6, 5, [1,2,2,1],2) #11
	W = state(7, 3, [1,3,2,1],1) #10g
	E = state(7, 3, [1,3,2,1],3) #10
	
	#=
	Wc = state(6, 8, [0,3,2,1],1) #14
	Wr = state(7, 4, [1,3,2,1],1) #11
	We = state(7, 6, [1,3,2,1],2) #13
	
	Er = state(7, 4, [1,3,2,1],3) #11
	Ec = state(6, 8, [1,3,1,1],3) #14
	Ee = state(7, 6, [1,3,2,1],4) #13
	Ew = state(7, 6, [1,3,2,1],2) #13
	=#
	
	Cr = state(6, 6, [1,2,2,1],2) #12 
	Cw = state(6, 8, [1,2,2,1],1) #14
	Ce = state(6, 8, [1,2,2,1],3) #14
	#done ^
	
	#=
	WrC = state(6, 9, [1,3,2,1],1) #15
	WrE = state(7, 7, [1,3,2,1],1) #14
	
	Er = state(7, 4, [1,3,2,1],3) #11
	Er = state(7, 4, [1,3,2,1],3) #11
	Er = state(7, 4, [1,3,2,1],3) #11
	Er = state(7, 4, [1,3,2,1],3) #11
	=#
	
	
	
	
	
	
	#Collect Action Branch
	#Cr = state(6, 6, [1,2,2,1],2) #12
	#Cw = state(6, 8, [1,2,2,1],1) #14
	#Ce = state(6, 8, [1,2,2,1],3) #14

	CrC = state(5, 11, [1,1,2,1],2) #16 =>
	CrW = state(6, 9,  [1,2,2,1],1) #15
	CrE = state(6, 9,  [1,2,2,1],3) #15
	
	CrCr = state(5, 12, [1,1,2,1],2) #17 =>
	CrCw = state(5, 14, [1,1,2,1],1) #19
	CrCe = state(5, 14, [1,1,2,1],3) #19
	
	CrCrC = state(4, 17, [1,0,2,1],2) #21 =>
	CrCrw = state(5, 15, [1,1,2,1],1) #20
	CrCre = state(5, 15, [1,1,2,1],3) #20
	
	CrCrCw = state(4, 20, [1,0,2,1],1) #24 * (who's first in line)
	CrCrCe = state(4, 20, [1,0,2,1],3) #24
	
	CrCrCwC = state(3, 25, [0,0,2,1],1) #28 =>
	CrCrCwR = state(4, 21, [1,0,2,1],1) #25
	CrCrCwE = state(4, 23, [1,0,2,1],2) #27
	
	
	CrCrCwCe = state(3, 28, [0,0,2,1],1) #31 => 
	CrCrCwCw = state(3, 28, [0,0,2,1],1) #31 Cant go west if neighbourA = F
	CrCrCwCr = state(3, 26, [0,0,2,1],1) #29 Cannot remain if Office = 0
	
	CrCrCwCeW = state(3, 31, [0,0,2,1],1) #29 check if Next_Office != Prev_Office
	CrCrCwCeE = state(3, 31, [0,0,2,1],3) #29 =>
	
	
	CrCrCwCeEc = state(2, 36, [0,0,1,1],3) #38 
	CrCrCwCeEr = state(3, 32, [0,0,2,1],3) #35 
	CrCrCwCeEe = state(3, 34, [0,0,2,1],3) #37 
	CrCrCwCeEw = state(3, 34, [0,0,2,1],3) #37 check if Next_Office != Prev_Office
	
	CrCrCwCeEcR = state(2, 37, [0,0,1,1],3) #39
	CrCrCwCeEcE = state(2, 39, [0,0,1,1],3) #41
	CrCrCwCeEcW = state(2, 39, [0,0,1,1],3) #41
	CrCrCwCeEcC = state(1, 41, [0,0,0,1],3) #42 Cannot Collect twice
	
	CrCrCwCeEcRc = state(1, 42, [0,0,0,1],3) #43 => 
	CrCrCwCeEcRe = state(2, 40, [0,0,1,1],3) #42
	CrCrCwCeEcRw = state(2, 40, [0,0,1,1],3) #42
	
	CrCrCwCeEcRcE = state(1, 45, [0,0,0,1],4) #46 => 
	CrCrCwCeEcRcW = state(1, 45, [0,0,0,1],2) #46 Preference to Office With Items
	CrCrCwCeEcRcR = state(1, 43, [0,0,0,1],3) #46 Cannot remain if Office = 0
	
	
	
	CrCrCwCeEcRcEc = state(0, 50, [0,0,0,0],4) #50 => Goal Reached, End Gracefully...
	
	CrCrCwCeEcRcEr = state(1, 46, [0,0,0,1],4) #47 
	CrCrCwCeEcRcEe = state(1, 48, [0,0,0,1],4) #49 => Cant go west if neighbourA = F
	CrCrCwCeEcRcEw = state(1, 48, [0,0,0,1],4) #48 
	
	
	
	#CrCrCwEc = state(1, 23, [1,0,1,1],3) #25 neighbourB
	#CrCrCwER = state(2, 23, [1,0,2,1],3) #25
	#CrCrCwEe = state(2, 23, [1,0,2,1],3) #25
	
	
	
	#CwCr = state(5, )
	
end
=#

# ╔═╡ 6cfc0bc9-3df6-48b5-aa6a-d7dabe937f87
# classList = list(1,2,3)

# ╔═╡ 0781e581-78bd-4ea1-83f7-3573de6f0119

#begin
	
	#function a_StarSearch(Action, Office)
#Julia has no switch statemnt so this will deffo be a pain			
	#if Action.Name = "Remain"
	#		currentBalance = currentBalance + 1
	#elseif Action.Name = "Move East"
	#	CurrentBalance = CurrentBalance + 3
	#
	#if
		
		

# ╔═╡ Cell order:
# ╠═72641af2-af36-11eb-060b-377776bf04a4
# ╠═1d119b9b-138f-4465-b866-b7383aa69312
# ╟─286ec198-8005-44e5-b3fb-8e54f48b6709
# ╟─8b9602bc-9ef3-410a-9379-079e373cb375
# ╟─404acaa3-9b99-47f5-8279-4fa7fd2fdf74
# ╠═e834f65e-0db3-4f84-bfd4-f6650c65312a
# ╠═fdd6e435-cf61-4720-92d9-270f545baca1
# ╠═d4776e5f-5a53-479c-8965-331f91380f73
# ╠═6b7b89bc-4eb6-42e8-ac8c-fc9e05a04828
# ╠═805542fa-ff1e-4bc8-9a18-c6fe1ee91fda
# ╠═89b1320e-27cc-41d1-9028-4d641aa593b6
# ╠═6ab1f0f3-de2d-4b8b-93fb-185dc30e073c
# ╠═338783ab-1ea8-4956-883d-14d9a39af543
# ╠═62553898-4d5f-41a4-a07d-87db9f5b4519
# ╠═4e8d81dc-6e9f-42de-9ef9-24664ada11b5
# ╠═281ee45c-20ca-4e43-a885-7f78987acb44
# ╠═7cb1530a-4e45-4513-8f62-69ca5d33dc43
# ╠═307ffb1a-edfb-4bcf-a675-43315d01b700
# ╟─38cd1fc8-b6e8-4fab-a034-95b977bc3c27
# ╟─47ae0816-9368-43e3-b153-6cff476612ed
# ╟─b1e9d358-e009-46ad-8d31-f00af4d4cdae
# ╠═74950239-9b5e-4110-ae4f-31478770d8c5
# ╠═6cfc0bc9-3df6-48b5-aa6a-d7dabe937f87
# ╠═0781e581-78bd-4ea1-83f7-3573de6f0119
